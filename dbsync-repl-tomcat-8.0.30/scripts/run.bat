call "setenv.bat"

set CONFIG_DIR=%SYNCH_HOME%/dbsync-repl/WEB-INF/db/conf

set JAVA_HOME=%SYNCH_HOME%\jre1.8.0_65


set CR=%SYNCH_HOME%\dbsync-repl\WEB-INF\lib

#%SYNCH_HOME%\WEB-INF\classes;

set CLASSPATH=.;%CR%\aws-java-sdk-core-1.9.0.jar;%CR%\aws-java-sdk-redshift-1.10.29.jar;%CR%\aws-java-sdk-s3-1.9.0.jar;%CR%\axis.jar;%CR%\cassandra-all-1.2.19.jar;%CR%\cassandra-driver-core-3.1.0-shaded.jar;%CR%\cassandra-driver-extras-3.1.0.jar;%CR%\cassandra-driver-mapping-3.1.0.jar;%CR%\cassandra-jdbc-1.2.5.jar;%CR%\cassandra-thrift-3.5.jar;%CR%\commons-beanutils.jar;%CR%\commons-codec-1.6.jar;%CR%\commons-collections-3.1.jar;%CR%\commons-collections-3.1.jar;%CR%\commons-dbcp-1.2.2.jar;%CR%\commons-digester.jar;%CR%\commons-discovery-0.2.jar;%CR%\commons-httpclient-3.0-rc4.jar;%CR%\commons-lang-2.1.jar;%CR%\commons-logging-1.1.1.jar;%CR%\commons-pool-1.1.jar;%CR%\dbsync-repl-engine-3.0.jar;%CR%\force-wsc-36.3.0.jar;%CR%\force-wsdl.jar;%CR%\gson-2.2.4.jar;%CR%\guava-19.0.jar;%CR%\httpclient-4.3.3.jar;%CR%\httpcore-4.3.jar;%CR%\jackson-annotations-2.7.4.jar;%CR%\jackson-core-2.7.4.jar;%CR%\jackson-core-asl-1.9.13.jar;%CR%\jackson-databind-2.7.4.jar;%CR%\jackson-mapper-asl-1.9.13.jar;%CR%\jaxrpc.jar;%CR%\jdom.jar;%CR%\joda-time-2.9.jar;%CR%\libthrift-0.9.3.jar;%CR%\log4j-1.2.15.jar;%CR%\mail.jar;%CR%\metrics-core-3.1.2.jar;%CR%\mysql-connector-java-5.1.24-bin.jar;%CR%etty-handler-4.1.2.Final.jar;%CR%\ojdbc6.jar;%CR%\ostermillerutils_1_05_00_for_java_1_4.jar;%CR%\postgresql-8.3dev-600.jdbc2.jar;%CR%\slf4j-api-1.5.10.jar;%CR%\slf4j-log4j12-1.5.10.jar;%CR%\slf4j-simple-1.5.2.jar;%CR%\sqljdbc4.jar;%CR%\wsdl4j-1.5.1.jar;

%JAVA_HOME%\bin\java -Xmx512m -Dinstance=%1 -Dconfig.dir=%CONFIG_DIR% -classpath %CLASSPATH% com.dbsync.salesforce.dbsynch.Driver %2 %3 %4

